module Main where

import           Control.Monad             (void, when)
import           Linear                    (V2 (..), V3 (..), V4 (..))

-- these two packages export a lot of
-- generic names like 'clear' or 'initialize'
-- so they're better used qualified
import           Graphics.Rendering.OpenGL (($=))
import qualified Graphics.Rendering.OpenGL as GL
import qualified SDL

import           Data.Foldable             (foldl')
import           Renderer.Sprite           (createSpriteModel, renderOneSprite)
import           Renderer.Texture


-- | We want a window with OpenGL capabilities
windowConfig :: SDL.WindowConfig
windowConfig = SDL.defaultWindow
  { SDL.windowOpenGL = Just openGLConfig
  , SDL.windowInitialSize = V2 1280 720
  }

-- | We want OpenGL 4.5, Core profile, with debugging
openGLConfig :: SDL.OpenGLConfig
openGLConfig = SDL.defaultOpenGL
  { SDL.glProfile = SDL.Core SDL.Debug 4 5
  }

wasButtonPressed :: SDL.Keycode -> SDL.KeyboardEventData -> Bool
wasButtonPressed kc kbd = SDL.keyboardEventKeyMotion kbd == SDL.Pressed
  && SDL.keysymKeycode (SDL.keyboardEventKeysym kbd) == kc

-- | Repeat action until its result is false
whileM :: Monad m => m Bool -> m ()
whileM m = m >>= flip when (whileM m)

getSpriteUVRect :: Int -> Int -> Int -> Int -> V4 Float
getSpriteUVRect w h x y = V4 minU minV lenU lenV where
  minU = pixelToU $ x * 16 + x
  minV = pixelToV $ y * 16 + y + 1
  lenU = pixelToU 16
  lenV = pixelToV 16
  pixelToU = (/fromIntegral w) . fromIntegral
  pixelToV = (/fromIntegral h) . fromIntegral

whileWithM :: Monad m => a -> (a -> m (a, Bool)) -> m [a]
whileWithM x f = do
  (x', cont) <- f x
  if cont
  then fmap (x':) $ whileWithM x' f
  else return [x']

whileWithM_ :: Monad m => a -> (a -> m (a, Bool)) -> m ()
whileWithM_ x f = void $ whileWithM x f


handleEvents :: (Int, Int) -> [SDL.Event] -> ((Int, Int), Bool)
handleEvents p = foldl' handleEvent (p, True) where
  handleEvent v@(p'@(x,y), notQuit) ev = case SDL.eventPayload ev of
    SDL.QuitEvent -> (p', False)
    SDL.KeyboardEvent kbd
      | wasButtonPressed SDL.KeycodeEscape kbd -> (p', False)
      | wasButtonPressed SDL.KeycodeLeft kbd -> ((x - 1, y), notQuit)
      | wasButtonPressed SDL.KeycodeRight kbd -> ((x + 1, y), notQuit)
      | wasButtonPressed SDL.KeycodeDown kbd -> ((x, y + 1), notQuit)
      | wasButtonPressed SDL.KeycodeUp kbd -> ((x, y - 1), notQuit)
      | otherwise -> v
    _  -> v

mainLoop :: SDL.Window -> IO ()
mainLoop w = do
  GL.debugOutput $= GL.Enabled
  GL.debugMessageCallback $= Just print
  model <- createSpriteModel
  let id33 = V3
        (V3 (0.5 * 720/1280) 0 0)
        (V3 0 0.5 0)
        (V3 0 0 1)
  tex <- readTexture "assets/spritesheets/roguelikeChar_transparent.png"
  let posToSpriteUVRect = getSpriteUVRect (textureWidth tex) (textureHeight tex)
  whileWithM_ (0, 0) $ \p@(x, y) -> do
    events <- SDL.pollEvents
    let bgColor = GL.Color4 0.9 0.7 0.5 1.0
        rect = posToSpriteUVRect x y
    print rect
    GL.clearBuffer (GL.ClearColorBufferFloat 0 bgColor)
    renderOneSprite id33 -0.5 rect (textureObject tex) model
    SDL.glSwapWindow w
    return $ handleEvents p events

main :: IO ()
main = do
  SDL.initialize [SDL.InitVideo]
  window <- SDL.createWindow "hello world" windowConfig
  ctx <- SDL.glCreateContext window
  mainLoop window
  SDL.glDeleteContext ctx
  return ()
