-- | Implementing some strangely missing stuff
module Algorithms where

import           Control.Monad           (when)
import           Control.Monad.Primitive (PrimMonad, PrimState)
import           Control.Monad.ST
import           Data.Vector             (Vector)
import qualified Data.Vector             as V
import           Data.Vector.Mutable     (MVector)
import qualified Data.Vector.Mutable     as MV

-- | Copies the vector and then does an in-place quicksort
-- | I couldn't find this in the standard vector package and the
-- | one I *did* find that has it was 4 years old
quicksort :: Ord a => Vector a -> Vector a
quicksort = quicksortBy compare

-- | As quicksort, but with a custom ordering
quicksortBy :: (a -> a -> Ordering) -> Vector a -> Vector a
quicksortBy cmp v = runST $ do
  mv <- V.thaw v
  inPlaceQuicksortBy cmp mv
  V.unsafeFreeze mv

-- | Quicksort a vector in place
inPlaceQuicksort :: (PrimMonad m, Ord a) => MVector (PrimState m) a -> m ()
inPlaceQuicksort = inPlaceQuicksortBy compare

-- | As inPlaceQuicksort, but with a custom ordering
inPlaceQuicksortBy :: (PrimMonad m) => (a -> a -> Ordering) -> MVector (PrimState m) a -> m ()
inPlaceQuicksortBy cmp v = when (len > 1) $ do
  -- Haskell is known as the finest imperative language
  pivotLoc <- pivotize
  when (pivotLoc > 1) .
    inPlaceQuicksortBy cmp $ sliceFromTo 0 (pivotLoc - 1)
  when (pivotLoc < len - 1) .
    inPlaceQuicksortBy cmp $ sliceFromTo (pivotLoc + 1) (len - 1)
  where
    len = MV.length v
    sliceFromTo i j = MV.slice i (j - i + 1) v
    pivotize = do
      MV.swap v 0 (len `div` 2)
      pivotize' 0 0
    pivotize' i pivotLoc =
      if i < len
      then do
        pivot <- MV.read v pivotLoc
        x <- MV.read v i
        if cmp x pivot == LT
        then do
          let newPivotLoc = pivotLoc + 1
          MV.swap v i pivotLoc
          MV.swap v i newPivotLoc
          pivotize' (i+1) newPivotLoc
        else
          pivotize' (i+1) pivotLoc
      else return pivotLoc
