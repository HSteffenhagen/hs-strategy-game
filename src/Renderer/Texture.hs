module Renderer.Texture
  ( Texture(..)
  , readTexture
  )
where

import qualified Codec.Picture             as Juicy
import qualified Data.Vector.Storable      as SV
import           Data.Word                 (Word8)
import           Foreign.ForeignPtr        (withForeignPtr)
import           Foreign.Marshal.Array     (mallocArray)
import           Foreign.Marshal.Utils     (copyBytes)
import           Foreign.Ptr               (castPtr)
import           Foreign.Storable          (sizeOf)
import           Graphics.Rendering.OpenGL (($=))
import qualified Graphics.Rendering.OpenGL as GL

data Texture = Texture
  { textureObject :: GL.TextureObject
  , textureWidth  :: Int
  , textureHeight :: Int
  }

withOpenGLPixels :: Juicy.Image Juicy.PixelRGBA8 -> (GL.PixelData Word8 -> IO a) -> IO a
withOpenGLPixels img usePixels = do
  let pixels = Juicy.imageData img
      (fpPixels, len) = SV.unsafeToForeignPtr0 pixels
  withForeignPtr fpPixels $ \pPixels ->
    usePixels $ GL.PixelData GL.RGBA GL.UnsignedByte (castPtr pPixels)

readTexture :: FilePath -> IO Texture
readTexture path = do
  tex <- GL.genObjectName
  GL.textureBinding GL.Texture2D $= Just tex
  -- clamping is just very conspicuous. We never actually want this to happen
  -- but doing it like this makes it more apparent when it does
  GL.textureWrapMode GL.Texture2D GL.S $= (GL.Repeated, GL.ClampToEdge)
  GL.textureWrapMode GL.Texture2D GL.T $= (GL.Repeated, GL.ClampToEdge)
  -- we don't care about mip-mapping for now, and sprite graphics tend
  -- to look way better with the nearest filter (anything else is going
  -- to look very blurry for low resolution textures)
  GL.textureFilter GL.Texture2D $= ((GL.Nearest, Nothing), GL.Nearest)
  -- we're lazy so we just convert input via Juicy if we have to
  -- it'd *probably* be better to just reject non-RGBA8 input images
  -- but I really can't be arsed to implement the logic for that
  -- TODO: Better error handling rather than just errorring out
  img <- Juicy.convertRGBA8 . either error id <$> Juicy.readImage path
  let w = Juicy.imageWidth img
      h = Juicy.imageHeight img
  withOpenGLPixels img $ \pixels ->
    GL.texImage2D GL.Texture2D GL.NoProxy 0 GL.RGBA' (GL.TextureSize2D (fromIntegral w) (fromIntegral h)) 0 pixels
  GL.textureBinding GL.Texture2D $= Nothing
  return Texture
    { textureObject = tex
    , textureWidth = w
    , textureHeight = h
    }
