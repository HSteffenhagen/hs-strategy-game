-- | Rendering batches of sprite in OpenGL
module Renderer.Sprite
where

import           Control.Monad                    (when)
import           Control.Monad.IO.Class
import qualified Data.ByteString                  as B
import           Data.Foldable                    (traverse_)
import qualified Data.Text                        as T
import qualified Data.Text.Encoding               as T
import qualified Data.Vector                      as V
import qualified Data.Vector.Storable             as SV
import           Data.Word                        (Word32)
import           Foreign.ForeignPtr               (withForeignPtr)
import qualified Foreign.Ptr                      as Ptr
import           Foreign.Storable                 (Storable (..))
import           Graphics.Rendering.OpenGL        (($=))
import qualified Graphics.Rendering.OpenGL        as GL
import           Linear                           (M33 (..), V2 (..), V3 (..),
                                                   V4 (..))
import           Linear.Affine                    (Point (..))
import           Renderer.Shader
import qualified Text.Shakespeare.Text            as Shakespeare

import           Graphics.Rendering.OpenGL.Linear (UniformMatrix (..))

data Sprite = Sprite
  { spriteTexture :: GL.TextureObject
  , spriteOffset  :: Point V2 Int
  , spriteSize    :: V2 Int
  }

data DrawSpriteCommand = DrawSpriteCommand
  { drawSpriteCommandSprite    :: Sprite
  , drawSpriteCommandTransform :: M33 Float
  , drawSpriteCommandLayer     :: Int
  }

newtype SpriteBatch = SpriteBatch { spriteBatchCommands :: V.Vector DrawSpriteCommand }

data Vertex = Vertex
  { vertexPos :: Point V2 Float
  , vertexUV  :: V2 Float
  }

instance Storable Vertex where
  sizeOf v = sizeOf (vertexPos v) + sizeOf (vertexUV v)
  alignment v = max (alignment (vertexPos v)) (alignment (vertexUV v))
  peek ptr = Vertex
    <$> peek (Ptr.plusPtr ptr 0)
    <*> peek (Ptr.plusPtr ptr (sizeOfSingle @(Point V2 Float)))
  poke ptr v = do
    poke (Ptr.plusPtr ptr 0) $ vertexPos v
    poke (Ptr.plusPtr ptr $ sizeOfSingle @(Point V2 Float)) $ vertexUV v

data SpriteModelBuffer = SpriteModelBuffer
  { spriteModelVertexBuffer :: GL.BufferObject
  , spriteModelIndexBuffer  :: GL.BufferObject
  }

sizeOfSingle :: forall a. Storable a => Int
sizeOfSingle = sizeOf @a undefined

-- | Write vector data into a target buffer (with usage = static_draw)
writeVectorData :: forall a. Storable a => GL.BufferTarget -> SV.Vector a -> IO ()
writeVectorData buf v = do
  let (ptr, len) = SV.unsafeToForeignPtr0 v
  withForeignPtr ptr $ \vptr ->
    GL.bufferData buf $=
      ( fromIntegral $ sizeOfSingle @a * len
      , vptr
      , GL.StaticDraw
      )

-- | Create the buffers used for sprite models
--
-- will set array and element array buffers to nil
createSpriteModelBuffer :: IO SpriteModelBuffer
createSpriteModelBuffer = do
  -- create and fill vertex buffer
  vertexBuffer <- GL.genObjectName
  GL.bindBuffer GL.ArrayBuffer $= Just vertexBuffer
  writeVectorData GL.ArrayBuffer vertexData
  GL.bindBuffer GL.ArrayBuffer $= Nothing

  -- create and fill index buffer
  indexBuffer <- GL.genObjectName
  GL.bindBuffer GL.ElementArrayBuffer $= Just indexBuffer
  writeVectorData GL.ElementArrayBuffer indexData
  GL.bindBuffer GL.ElementArrayBuffer $= Nothing
  return $ SpriteModelBuffer
    { spriteModelVertexBuffer = vertexBuffer
    , spriteModelIndexBuffer = indexBuffer
    }
  where
    -- a 2d rectangle
    vertexData = SV.fromList
      [ Vertex (P $ V2 -1 -1) (V2 0 1) -- bottom left
      , Vertex (P $ V2 1 -1) (V2 1 1) -- bottom right
      , Vertex (P $ V2 1 1) (V2 1 0) -- top right
      , Vertex (P $ V2 -1 1) (V2 0 0) -- top left
      ]
    -- note: Indices *must* be unsigned
    indexData = SV.fromList @Word32
      [ 0, 1, 2 -- bottom right corner
      , 2, 3, 0 -- top left corner
      ]

destroySpriteModelBuffer :: SpriteModelBuffer -> IO ()
destroySpriteModelBuffer buffer = do
  GL.deleteObjectName $ spriteModelVertexBuffer buffer
  GL.deleteObjectName $ spriteModelIndexBuffer buffer

data SpriteModel = SpriteModel
  { spriteModelVao    :: GL.VertexArrayObject
  , spriteModelBuffer :: SpriteModelBuffer
  , spriteModelShader :: GL.Program
  }

destroySpriteModel :: SpriteModel -> IO ()
destroySpriteModel model = do
  GL.deleteObjectName $ spriteModelVao model
  destroySpriteModelBuffer $ spriteModelBuffer model
  GL.deleteObjectName $ spriteModelShader model

createSpriteModel :: IO SpriteModel
createSpriteModel = do
  when (sizeOfSingle @Float /= 4) $ error ":("
  -- yep, this is what it takes to render a rectangle in OpenGL
  -- seems legit
  vao <- GL.genObjectName
  GL.bindVertexArrayObject $= Just vao
  -- initial object creation
  buffer <- createSpriteModelBuffer
  vertexShader <- createShader vertexShader2DSource
  fragmentShader <- createShader fragmentShader2DSource
  let shaders = [vertexShader, fragmentShader]
  program <- linkProgram shaders
  traverse_ GL.deleteObjectName shaders
  -- attribute setup
  GL.currentProgram $= Just program
    -- this is the part I always screw up somehow
  -- we only need to do this once because this stuff
  -- is saved in the VAO
  GL.bindBuffer GL.ArrayBuffer $= Just (spriteModelVertexBuffer buffer)
  GL.bindBuffer GL.ElementArrayBuffer $= Just (spriteModelIndexBuffer buffer)
  GL.vertexAttribPointer shader2DPositionLocation $=
    ( GL.ToFloat -- don't normalise
    , GL.VertexArrayDescriptor
        2 -- a 2d vector
        GL.Float -- of floats
        (fromIntegral $ sizeOfSingle @Vertex) -- one vertex of space between each
        Ptr.nullPtr -- starting at index 0
    )
  GL.vertexAttribArray shader2DPositionLocation $= GL.Enabled
  GL.vertexAttribPointer shader2DUVLocation $=
    ( GL.ToFloat -- don't normalise
    , GL.VertexArrayDescriptor
        2 -- a 2d vector
        GL.Float -- of floats
        (fromIntegral $ sizeOfSingle @Vertex) -- one vertex of space between each
        (Ptr.plusPtr Ptr.nullPtr 8) -- starting after the 2D pos
    )
  GL.vertexAttribArray shader2DUVLocation $= GL.Enabled
  -- clean up after ourselves
  GL.bindVertexArrayObject $= Nothing
  GL.currentProgram $= Nothing
  return SpriteModel
    { spriteModelVao = vao
    , spriteModelBuffer = buffer
    , spriteModelShader = program
    }

-- | render a single 'sprite' (actually just a rectangle for now)
--
--   only here for convenience purposes it's very inefficient to not batch draw
--   calls
renderOneSprite :: M33 Float -> Float -> V4 Float -> GL.TextureObject -> SpriteModel -> IO ()
renderOneSprite pixelToScreen zOffset (V4 u v ul vl) tex model = do
  GL.bindVertexArrayObject $= Just (spriteModelVao model)
  GL.currentProgram $= Just (spriteModelShader model)
  GL.uniform shader2DPixelToScreenLocation $= UniformMatrix pixelToScreen
  GL.uniform shader2DZOffsetLocation $= zOffset
  GL.uniform shader2DUVRectLocation $= GL.Vector4 u v ul vl
  GL.textureBinding GL.Texture2D $= Just tex
  GL.uniform shader2DTextureLocation $= GL.TextureUnit 0
  GL.drawElements GL.Triangles 6 GL.UnsignedInt Ptr.nullPtr
