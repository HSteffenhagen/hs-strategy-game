{-# LANGUAGE QuasiQuotes #-}

module Renderer.Shader
where

import           Control.Exception
import           Control.Monad             (when)
import           Control.Monad.IO.Class
import qualified Data.ByteString           as B
import           Data.Foldable             (traverse_)
import qualified Data.Text                 as T
import qualified Data.Text.Encoding        as T
import           Graphics.Rendering.OpenGL (($=))
import qualified Graphics.Rendering.OpenGL as GL
import           Linear                    (M33 (..), (!*!))
import qualified Text.Shakespeare.Text     as Shakespeare
import           Type.Reflection           (Typeable)

-- | A shader source with statically known shader type
data KnownShaderSource (shaderType :: GL.ShaderType) = KnownShaderSource
  { sourceAsUtf8 :: B.ByteString }

-- | A shader source + a dynamic shader type
data DynamicShaderSource = DynamicShaderSource
  { dynamicShaderSourceUtf8 :: B.ByteString
  , dynamicShaderType       :: GL.ShaderType
  }

sourceFromText :: forall st. T.Text -> KnownShaderSource st
sourceFromText = KnownShaderSource . T.encodeUtf8

class ShaderSource a where
  shaderSourceAsUtf8 :: a -> B.ByteString
  getShaderType :: a -> GL.ShaderType

instance ShaderSource (KnownShaderSource GL.VertexShader) where
  shaderSourceAsUtf8 = sourceAsUtf8
  getShaderType _ = GL.VertexShader

instance ShaderSource (KnownShaderSource GL.FragmentShader) where
  shaderSourceAsUtf8 = sourceAsUtf8
  getShaderType _ = GL.FragmentShader

makeDynamicShaderSource :: GL.ShaderType -> T.Text -> DynamicShaderSource
makeDynamicShaderSource st src = DynamicShaderSource
  { dynamicShaderSourceUtf8 = T.encodeUtf8 src
  , dynamicShaderType = st
  }

instance ShaderSource DynamicShaderSource where
  shaderSourceAsUtf8 = dynamicShaderSourceUtf8
  getShaderType = dynamicShaderType


newtype ShaderCreationException = ShaderCreationException T.Text
  deriving (Typeable, Show)

instance Exception ShaderCreationException

createShader :: ShaderSource shaderSource => shaderSource -> IO GL.Shader
createShader shaderSource = do
  shader <- GL.createShader (getShaderType shaderSource)
  GL.shaderSourceBS shader $= shaderSourceAsUtf8 shaderSource
  GL.compileShader shader
  compileOk <- GL.get (GL.compileStatus shader)
  when (not compileOk) $ do
    infoLog <- GL.get (GL.shaderInfoLog shader)
    GL.deleteObjectName shader
    throw . ShaderCreationException $ T.pack infoLog
  return shader

newtype ProgramCreationException = ProgramCreationException T.Text
  deriving (Typeable, Show)

instance Exception ProgramCreationException

linkProgram :: [GL.Shader] -> IO GL.Program
linkProgram shaders = do
  program <- GL.createProgram
  traverse_ (GL.attachShader program) shaders
  GL.linkProgram program
  linkOk <- GL.get (GL.linkStatus program)
  when (not linkOk) $ do
    infoLog <- GL.get (GL.programInfoLog program)
    GL.deleteObjectName program
    throw . ProgramCreationException $ T.pack infoLog
  traverse_ (GL.detachShader program) shaders
  return program

-- ###############################################
-- Stuff for the 'default' 2D shader
-- i.e. 2D vertices + UV, load color from texture
-- ###############################################

shader2DPixelToScreenLocation :: GL.UniformLocation
shader2DPixelToScreenLocation = GL.UniformLocation 0

shader2DZOffsetLocation :: GL.UniformLocation
shader2DZOffsetLocation = GL.UniformLocation 1

shader2DUVRectLocation :: GL.UniformLocation
shader2DUVRectLocation = GL.UniformLocation 2

shader2DTextureLocation :: GL.UniformLocation
shader2DTextureLocation = GL.UniformLocation 3

shader2DPositionLocation :: GL.AttribLocation
shader2DPositionLocation = GL.AttribLocation 0

shader2DUVLocation :: GL.AttribLocation
shader2DUVLocation = GL.AttribLocation 1


vertexShader2DSource :: KnownShaderSource GL.VertexShader
vertexShader2DSource = sourceFromText [Shakespeare.st|
#version 450

layout(location=0) uniform mat3 pixelToScreen;
layout(location=1) uniform float zOffset;
layout(location=2) uniform vec4 uvRect;

layout(location=0) in vec2 position;
layout(location=1) in vec2 uv;

out vec2 fragmentUV;

void main() {
  fragmentUV = uvRect.xy + uv * uvRect.zw;
  vec2 pos = (pixelToScreen * vec3(position, 1.0)).xy;
  gl_Position = vec4(pos, zOffset, 1.0);
}
|]

-- | Should use UV to sample texture; Right now only uses them to display color directly
fragmentShader2DSource :: KnownShaderSource GL.FragmentShader
fragmentShader2DSource = sourceFromText [Shakespeare.st|
#version 450

layout(location=3) uniform sampler2D tex;

in vec2 fragmentUV;

out vec4 color;
void main() {
  color = texture(tex, fragmentUV);
}
|]
