module Graphics.Rendering.OpenGL.Linear
where

import           Control.Exception
import           Control.Monad             (when)
import           Data.StateVar
import           Foreign.Marshal.Alloc     (alloca)
import           Foreign.Ptr               (castPtr)
import           Foreign.Storable
import           Graphics.GL.Core45        (pattern GL_CURRENT_PROGRAM,
                                            glGetIntegerv, glGetUniformfv,
                                            glUniformMatrix3fv)
import qualified Graphics.Rendering.OpenGL as GL
import           Linear
import           Type.Reflection

newtype MatrixUniformException = MatrixUniformException String
  deriving (Show, Typeable)

instance Exception MatrixUniformException

-- | A wrapper type for "Linear" matrices
--
-- The OpenGL package does not have a builtin matrix type,
-- and the linear package on the other hand doesn't depend on
-- OpenGL so of course doesn't provide an uniform instance for
-- its matrix types, this is a wrapper to provide that instance
newtype UniformMatrix m = UniformMatrix { getMatrix :: m }

setUniformMatrix3 ::  GL.UniformLocation -> M33 Float -> IO ()
setUniformMatrix3 (GL.UniformLocation loc) m =
  alloca $ \pmat -> do
    poke pmat m
    let count = 1
        transpose = 1
    glUniformMatrix3fv loc count transpose (castPtr pmat)

getUniformMatrix3 :: GL.UniformLocation -> IO (M33 Float)
getUniformMatrix3 (GL.UniformLocation loc) =
  alloca $ \pmat ->
  alloca $ \pprogram -> do
    glGetIntegerv GL_CURRENT_PROGRAM pprogram
    program <- fromIntegral <$> peek pprogram
    when (program == 0) $ do
      throw $ MatrixUniformException "getUniformMatrix3: No program bound"
    glGetUniformfv program loc (castPtr pmat)
    peek pmat

instance GL.Uniform (UniformMatrix (M33 Float)) where
  uniform loc = StateVar
    (UniformMatrix <$> getUniformMatrix3 loc)
    (\(UniformMatrix m) -> setUniformMatrix3 loc m)
