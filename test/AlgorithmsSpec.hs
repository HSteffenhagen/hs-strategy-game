module AlgorithmsSpec
where

import           Algorithms      (quicksort)
import           Data.List       (sort)
import qualified Data.Vector     as V
import           Test.Hspec
import           Test.QuickCheck

quicksortList = V.toList . quicksort . V.fromList

spec :: Spec
spec = do
  describe "Algorithms.quicksort" $ do
    it "should do the same thing as Prelude.sort" . property $
      (\(xs::[Int]) -> quicksortList xs == sort xs)
    it "should be able to sort a simple case" $ do
      quicksortList [3, 4, 9, 5, 7, 6, 2, 10, 1, 8] `shouldBe` [1..10]
