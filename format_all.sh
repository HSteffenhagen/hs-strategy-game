#!/bin/sh
base="$(dirname $(readlink -f $0))"
haskell_src_dirs="app src test packages/linear-uniform/src"
(cd "$base"; find $haskell_src_dirs -name '*.hs' -print0 | xargs -0 stack exec stylish-haskell -- -i)
